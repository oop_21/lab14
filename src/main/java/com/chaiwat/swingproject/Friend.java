/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chaiwat.swingproject;
import java.io.Serializable;
/**
 *
 * @author Chaiwat
 */
public class Friend implements Serializable{
    private String name;
    private int age;
    private String gender;
    private String descripthion;

    public Friend(String name, int age, String gender, String descripthion) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.descripthion = descripthion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescripthion() {
        return descripthion;
    }

    public void setDescripthion(String descripthion) {
        this.descripthion = descripthion;
    }

    @Override
    public String toString() {
        return "Friend{" + "name=" + name + ", age=" + age + ", gender=" + gender + ", descripthion=" + descripthion + '}';
    }
    
}
